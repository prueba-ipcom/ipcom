# Prueba Tecnica IPCOM

###Instalacion con Docker
Prueba Tecnica realizada con node y docker-compose.

## Compila Proyecto
docker-compose build

## Subir el contenedor
docker-compose up -d

## Cómo se usa

### Url
IPServer:8080
http://localhost:8080/

## Api Compras parte1:
http://localhost:8080/api/v1/resumen/2019-12-01?dias=5

## Api Archivo parte2:
http://localhost:8080/api/v1/archivo


### Se crean las 2 pruebas en el mismo proyecto con diferente endpoint.