const express = require("express");
const CompraController = require("../controllers/compras");

const api = express.Router();

api.get("/resumen/2019-12-01", CompraController.getCompras);

module.exports = api;
