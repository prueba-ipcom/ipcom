const express = require("express");
const ArchivoController = require("../controllers/archivos");

const api = express.Router();

api.get("/archivo", ArchivoController.readFile);

module.exports = api;
