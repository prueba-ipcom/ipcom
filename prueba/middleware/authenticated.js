const jwt = require('jwt-simple');
const moment = require('moment');

//Llave es la misma de Services
const SCRET_KEY = "4d6ddde01eeca961dce9062f271d1e73";

exports.ensureAuth = (req, res, next) => {

    if (!req.headers.authorization) {
        return res.status(403).send({ message: "La peticion no tiene cabecera de autenticacion" });
    }

    const token = req.headers.authorization.replace(/['"]+/g, "");

    try {
        var payload = jwt.decode(token, SCRET_KEY);

        if (payload.exp <= moment().unix()) {
            return res.status(401).send({ message: "Token ha expirado" });
        }
    } catch (ex) {
        console.log(ex);
        return res.status(401).send({ message: "Token invalido" });
    }

    req.user = payload;
    next();

}