const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TiendaHomologadaSchema = Schema({
    cdtienda: {
        type: String,
        unique: true
    },
    codigohomologado: {
        type: String,
        unique: true
    },
    estado: Boolean
});

module.exports = mongoose.model("Tienda", TiendaHomologadaSchema);

