const fetch = require("node-fetch");
const moment = require("moment");

getCompras = async (req, res) => {
  let fecha_ini = "2019-12-01";
  const format = "YYYY-MM-DD";
  let query = req.query;

  //   Valida que esxista el parametro dias en el request
  if (!query.dias) {
    res.status(401).send({ message: "Debe indicar los dias a buscar" });
  } else {
    // Crea arreglo con la url de los dias a consultar
    let arrDias = [];
    for (let i = 0; i < query.dias; i++) {
      arrDias[i] =
        "https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/" +
        moment(fecha_ini).add(i, "days").format(format);
    }
    // carga arreglo con el response de los dias a consultar
    let arrCompras = [];
    for (let i = 0; i < arrDias.length; i++) {
      arrCompras[i] = await getVentas(arrDias[i]);
    }
    // Concatena los arreglos con el response segun los dias a consultar
    var concatData = [];
    for (let i = 0; i < arrCompras.length; i++) {
      concatData = concatData.concat(arrCompras[i]);
    }
    // Filtra por objetos que contienen compra
    let filtraComprasTrue = await getFilteredByKey(concatData, "compro", true);
    // Filtra por objetos que NO contienen compra
    let filtraComprasFalse = await getFilteredByKey(
      concatData,
      "compro",
      false
    );
    let compraMasAlta = 0;
    // Calcula la compra mas alta
    compraMasAlta = Math.max(
      ...filtraComprasTrue.map((x) => parseFloat(x.monto))
    );

    let resultMonto = 0;
    // Setea la variable con el valor mas alto
    Math.resultMonto = filtraComprasTrue.filter(
      (x) => x.monto == compraMasAlta
    );
    if (compraMasAlta > resultMonto.monto) {
      compraMasAlta = resultMonto.monto;
    }

    // Carga la variable con la suma de las compras
    let vlrCompras = await sumarValores(filtraComprasTrue, "monto");

    // Crea Objeto con el mapeo de las diferentes tdc
    let objTdc = {};
    filtraComprasTrue.forEach((x) => {
      if (!objTdc.hasOwnProperty(x.tdc)) {
        // Setea el atributo segun la catidad de compras con una tdc
        objTdc[x.tdc] = getFilteredByKey(
          filtraComprasTrue,
          "tdc",
          x.tdc
        ).length;
      }
    });

    // Carga el objeto result con los calculos solicitados
    result = {
      total: Math.round(vlrCompras * 100) / 100,
      comprasPorTDC: objTdc,
      nocompraron: filtraComprasFalse.length,
      compraMasAlta: compraMasAlta,
    };

    // Envia respuesta JSON
    res.status(200).send({ resumen: result });
  }
};

//Consume endpoint de api de compras
getVentas = async (url) => {
  const params = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  return fetch(url, params)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      return err;
    });
};

//Funcion para validar que los datos a sumar se reciban en un objeto
validObj = async (obj) => {
  return Object.prototype.toString.call(obj) === "[object object]";
};
//Funcion para sumar los montos de las compras
sumarValores = async (compras, fn) => {
  if (!Array.isArray(compras)) {
    throw TypeError("Debe ser un arreglo");
  }

  if (!compras.every((d) => validObj(d))) {
    throw TypeError("El Arreglo debe contener unicamente objetos");
  }

  return compras
    .map(typeof fn === "function" ? fn : (d) => d[fn])
    .reduce((a, v) => a + v, 0);
};
//Funcion para para filtrar por llave - valor
function getFilteredByKey(array, key, value) {
  return array.filter(function (e) {
    return e[key] == value;
  });
}

module.exports = {
  getCompras,
};
