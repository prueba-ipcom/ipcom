const csv = require("csv-parser");
const fs = require("fs");
const moment = require("moment");

readFile = (req, res) => {
  let arrData = [];
  let dataFile = [];
  let objFile = {};
  const format = "YYYY-MM-DD HH:mm:ss";
  fs.createReadStream("./uploads/files/Daily_CDR_CallCenter_SA_HT0702202248.csv")
    .pipe(csv())
    .on("data", (row) => {
      let datoAmount = row.Amount.split(".");
      let datoSalesRate = row.SalesRate.split(".");
      var date = moment(row.Date_time).format(format);
      if (row.GroupID === 'SLVDID' || row.GroupID === 'DIDCOL') {

      } else {
        row.Amount = datoAmount[0];
        row.SalesRate = datoSalesRate[0];

        row.Date_transform = date;
        console.log(row);
        arrData.push(row);
      }

    })
    .on("end", () => {
      //console.log(arrData);
      // let orgObj = [];
      // for (const key in arrData) {
      //   if (Object.prototype.hasOwnProperty.call(arrData, key)) {
      //     const element = arrData[key];
      //     let foundOrg = orgObj.find(
      //       (org) => org.organization == element.organizacion
      //     );
      //     if (!foundOrg) {
      //       orgObj.push({
      //         organization: element.organizacion,
      //         users: [{ username: element.usuario, roles: [element.rol] }],
      //       });
      //     } else {
      //       let foundUser = foundOrg.users.find(
      //         (user) => user.username == element.usuario
      //       );
      //       if (!foundUser) {
      //         foundOrg.users.push({
      //           username: element.usuario,
      //           roles: [element.rol],
      //         });
      //       } else {
      //         let foundRole = foundUser.roles.find(
      //           (role) => role == element.rol
      //         );
      //         if (!foundRole) {
      //           foundUser.roles.push(element.rol);
      //         }
      //       }
      //     }
      //   }
      // }
      // res.status(200).send(arrData);
      console.log(arrData);
    });
};

module.exports = {
  readFile,
};
